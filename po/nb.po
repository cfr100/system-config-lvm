# Norwegian bokmål translation of system-config-lvm.
# Copyright (C) 2004-2005, Red Hat, Inc.
# This file is distributed under the same license as the system-config-lvm package.
# Kjartan Maraas <kmaraas@gnome.org>, 2004-2010.
#
msgid ""
msgstr ""
"Project-Id-Version: system-config-lvm 1.1.15\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-01-31 09:04-0500\n"
"PO-Revision-Date: 2010-10-24 13:03+0200\n"
"Last-Translator: Kjartan Maraas <kmaraas@gnome.org>\n"
"Language-Team: Norwegian bokmål <i18n-nb@lister.ping.uio.no>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/InputController.py:39 src/Properties_Renderer.py:24
#: src/Volume_Tab_View.py:32
#, python-format
msgid ""
"\n"
"  Unable to initialize graphical environment. Most likely cause of failure\n"
"  is that the tool was not run using a graphical environment. Please either\n"
"  start your graphical user interface or set your DISPLAY variable.\n"
"                                                                                \n"
"  Caught exception: %s\n"
msgstr ""

#. ##TRANSLATOR: The string below is seen when adding a new Physical
#. ##Volume to an existing Volume Group.
#: src/InputController.py:59
#, python-format
msgid "Select a Volume Group to add %s to:"
msgstr ""

#. ##TRANSLATOR: The two strings below refer to the name and type of
#. ##available disk entities on the system. There are two types --
#. ##The first is an 'unallocated physical volume' which is a disk or
#. ##partition that has been initialized for use with LVM, by writing
#. ##a special label onto the first block of the partition. The other type
#. ##is an 'uninitialized entity', which is an available disk or partition
#. ##that is NOT yet initialized to be used with LVM. Hope this helps give
#. ##some context.
#: src/InputController.py:87
msgid "Name"
msgstr "Navn"

#: src/InputController.py:88
msgid "Size"
msgstr "Størrelse"

#: src/InputController.py:89
msgid "Entity Type"
msgstr "Type entitet"

#: src/InputController.py:91
msgid "Unallocated Physical Volume"
msgstr "Ikke-allokert fysisk volum"

#: src/InputController.py:92
msgid "Uninitialized Disk Entity"
msgstr "Ikke-initiert diskentitet"

#: src/InputController.py:93
#, python-format
msgid "Select disk entities to add to the %s Volume Group:"
msgstr "Velg en diskentitet som skal legges til i volumgruppen %s:"

#: src/InputController.py:95
msgid ""
"A Volume Group must be made up of two or more Physical Volumes to support "
"striping. This Volume Group does not meet that requirement."
msgstr ""

#: src/InputController.py:97
#, python-format
msgid ""
"A Logical Volume with the name %s already exists in this Volume Group. "
"Please choose a unique name."
msgstr ""

#: src/InputController.py:99
#, python-format
msgid ""
"A Volume Group with the name %s already exists. Please choose a unique name."
msgstr ""

#: src/InputController.py:101
msgid "A Name must be provided for the new Logical Volume"
msgstr "Du må oppgi et navn for det nye logiske volumet"

#: src/InputController.py:103
msgid "A Name must be provided for the new Volume Group"
msgstr "Du må oppgi et navn for den nye volumgruppen"

#: src/InputController.py:105
#, python-format
msgid ""
"The specified mount point, %s, does not exist. Do you wish to create it?"
msgstr ""

#: src/InputController.py:107
#, python-format
msgid "The creation of mount point %s unexpectedly failed."
msgstr ""

#: src/InputController.py:109
msgid "This capability is not yet implemented in this version"
msgstr ""

#: src/InputController.py:111
msgid ""
"The number of Logical Volumes in this Volume Group has reached its maximum "
"limit."
msgstr ""

#: src/InputController.py:113
msgid ""
"The number of Physical Volumes in this Volume Group has reached its maximum "
"limit."
msgstr ""

#: src/InputController.py:115
#, python-format
msgid ""
"At most %s Physical Volumes can be added to this Volume Group before the "
"limit is reached."
msgstr ""

#: src/InputController.py:117
#, python-format
msgid ""
"Volume Group %s does not have enough space for new Logical Volumes. A "
"possible solution would be to add an additional Physical Volume to the "
"Volume Group."
msgstr ""

#: src/InputController.py:119
msgid "A snapshot of a snapshot is not supported."
msgstr ""

#: src/InputController.py:120
msgid "A snapshot of a mirrored Logical Volume is not supported."
msgstr ""

#: src/InputController.py:122
#, python-format
msgid ""
"Logical volume %s has snapshot %s currently associated with it. Please "
"remove the snapshot first."
msgstr ""

#: src/InputController.py:123
#, python-format
msgid ""
"Logical volume %s has snapshots: %s currently associated with it. Please "
"remove snapshots first."
msgstr ""

#: src/InputController.py:125
msgid ""
"Undefined type conversion error in model factory. Unable to complete task."
msgstr ""

#: src/InputController.py:127
#, python-format
msgid ""
"BIG WARNING: Logical Volume %s has an %s file system on it and is currently "
"mounted on %s. Are you absolutely certain that you wish to discard the data "
"on this mounted filesystem?"
msgstr ""

#: src/InputController.py:129
#, python-format
msgid ""
"Logical Volume %s is currently mounted on %s. In order to complete request, "
"it has to be unmounted. Are you sure you want it unmounted?"
msgstr ""

#. ##TRANSLATOR: An extent below is an abstract unit of storage. The size
#. ##of an extent is user-definable.
#: src/InputController.py:135
#, python-format
msgid "Unused space on %s"
msgstr "Ubrukt plass på %s"

#: src/InputController.py:136
#, python-format
msgid "%s megabytes"
msgstr "%s megabyte"

#: src/InputController.py:137
#, python-format
msgid "%s kilobytes"
msgstr "%s kilobyte"

#: src/InputController.py:138
#, python-format
msgid "%s gigabytes"
msgstr "%s gigabyte"

#: src/InputController.py:139
#, python-format
msgid "%s extents"
msgstr ""

#: src/InputController.py:141
msgid "Remaining free space in Volume Group:\n"
msgstr "Gjenværende ledig plass i volumgruppen:\n"

#: src/InputController.py:142
msgid "Remaining space for this Volume:\n"
msgstr "Gjenværende plass for dette volumet:\n"

#: src/InputController.py:144
msgid "Extents"
msgstr ""

#: src/InputController.py:145
msgid "Gigabytes"
msgstr "Gigabyte"

#: src/InputController.py:146
msgid "Megabytes"
msgstr "Megabyte"

#: src/InputController.py:147
msgid "Kilobytes"
msgstr "Kilobyte"

#: src/InputController.py:149
#, python-format
msgid "The %s should only contain number values"
msgstr ""

#: src/InputController.py:150
msgid ""
"The Maximum Physical Volumes field should contain only integer values "
"between 1 and 256"
msgstr ""

#: src/InputController.py:151
msgid ""
"The Maximum Logical Volumes field should contain only integer values between "
"1 and 256"
msgstr ""

#: src/InputController.py:153
#, python-format
msgid ""
"Are you quite certain that you wish to remove %s from Logical Volume "
"Management?"
msgstr ""

#: src/InputController.py:155
#, python-format
msgid ""
"The Physical Volume named %s, that you wish to remove, has data from active "
"Logical Volume(s) mapped to its extents. Because it is the only Physical "
"Volume in the Volume Group, there is no place to move the data to. "
"Recommended action is either to add a new Physical Volume before removing "
"this one, or else remove the Logical Volumes that are associated with this "
"Physical Volume."
msgstr ""

#: src/InputController.py:156
#, python-format
msgid ""
"Are you quite certain that you wish to remove %s from the %s Volume Group?"
msgstr ""

#: src/InputController.py:157
#, python-format
msgid ""
"Removing Physical Volume %s from the Volume Group %s will leave the Volume "
"group empty, and it will be removed as well. Do you wish to proceed?"
msgstr ""

#: src/InputController.py:158
#, python-format
msgid ""
"Volume Group %s does not have enough space to move the data stored on %s. A "
"possible solution would be to add an additional Physical Volume to the "
"Volume Group."
msgstr ""

#: src/InputController.py:159
msgid ""
"The dm-mirror module is either not loaded in your kernel, or your kernel "
"does not support the dm-mirror target. If it is supported, try running "
"\"modprobe dm-mirror\". Otherwise, operations that require moving data on "
"Physical Extents are unavailable."
msgstr ""

#: src/InputController.py:160
msgid ""
"The dm-snapshot module is either not loaded in your kernel, or your kernel "
"does not support the dm-snapshot target. If it is supported, try running "
"\"modprobe dm-snapshot\". Otherwise, creation of snapshots is unavailable."
msgstr ""

#: src/InputController.py:162
#, python-format
msgid "Are you quite certain that you wish to remove logical volume %s?"
msgstr ""

#: src/InputController.py:163
#, python-format
msgid ""
"Logical volume %s contains %s filesystem. All data on it will be lost! Are "
"you quite certain that you wish to remove logical volume %s?"
msgstr ""

#: src/InputController.py:164
#, python-format
msgid ""
"Logical volume %s contains data from directory %s. All data in it will be "
"lost! Are you quite certain that you wish to remove logical volume %s?"
msgstr ""

#: src/InputController.py:309
msgid ""
"In order for Volume Group to be safely used in clustered environment, lvm2-"
"cluster rpm has to be installed, `lvmconf --enable-cluster` has to be "
"executed and clvmd service has to be running"
msgstr ""

#: src/InputController.py:368
#, python-format
msgid ""
"Physical Volume %s contains extents belonging to a mirror log of Logical "
"Volume %s. Mirrored Logical Volumes are not yet migratable, so %s is not "
"removable."
msgstr ""

#: src/InputController.py:371
#, python-format
msgid ""
"Physical Volume %s contains extents belonging to a mirror image of Logical "
"Volume %s. Mirrored Logical Volumes are not yet migratable, so %s is not "
"removable."
msgstr ""

#: src/InputController.py:374
#, python-format
msgid ""
"Physical Volume %s contains extents belonging to %s, a snapshot of %s. "
"Snapshots are not yet migratable, so %s is not removable."
msgstr ""

#: src/InputController.py:379
#, python-format
msgid ""
"Physical Volume %s contains extents belonging to %s, the origin of snapshot %"
"s. Snapshot origins are not yet migratable, so %s is not removable."
msgstr ""

#: src/InputController.py:381
#, python-format
msgid ""
"Physical Volume %s contains extents belonging to %s, the origin of snapshots "
"%s. Snapshot origins are not yet migratable, so %s is not removable."
msgstr ""

#: src/InputController.py:568
#, python-format
msgid ""
"Logical Volume \"%s\" has snapshots that are not selected for removal. They "
"must be removed as well."
msgstr ""

#: src/InputController.py:583
#, python-format
msgid ""
"\"%s\", an origin of snapshot \"%s\", has been deleted from removal list."
msgstr ""

#: src/InputController.py:614
#, python-format
msgid ""
"Physical Volume \"%s\" contains extents belonging to a mirror. Mirrors are "
"not migratable, so %s is not removable."
msgstr ""

#: src/InputController.py:617
#, python-format
msgid ""
"Physical Volume \"%s\" contains extents belonging to a snapshot or a "
"snapshot's origin. Snapshots are not migratable, so %s is not removable."
msgstr ""

#: src/InputController.py:682
msgid "The path you specified does not exist."
msgstr ""

#: src/InputController.py:689
msgid "The path you specified is not a Block Device."
msgstr ""

#: src/InputController.py:760
#, python-format
msgid "Initialization of %s failed"
msgstr ""

#: src/InputController.py:998 src/InputController.py:1001
msgid "Please select some extents first"
msgstr ""

#: src/InputController.py:1035
msgid ""
"There are not enough free extents to perform the necessary migration. Adding "
"more physical volumes would solve the problem."
msgstr ""

#: src/InputController.py:1091
#, python-format
msgid ""
"iSCSI Initiator rpm is not installed. \n"
"Install %s rpm, and try again."
msgstr ""

#: src/InputController.py:1208
msgid "Migrate extents"
msgstr ""

#: src/InputController.py:1336
msgid "Options"
msgstr "Alternativer"

#: src/InputController.py:1370
#, python-format
msgid "Create A Snapshot of %s"
msgstr "Lag et øyeblikksbilde av %s"

#: src/InputController.py:1372
msgid "Create New Logical Volume"
msgstr "Opprett nytt logisk volum"

#: src/InputController.py:1375
#, python-format
msgid "Edit %s, a Snapshot of %s"
msgstr "Rediger %s, et bilde av %s"

#: src/InputController.py:1378
msgid "Edit Logical Volume"
msgstr "Rediger logisk volum"

#: src/InputController.py:1531
msgid "Underlying Logical Volume Management does not support mirroring"
msgstr ""

#: src/InputController.py:1538
msgid "Striped Logical Volumes cannot be mirrored."
msgstr ""

#: src/InputController.py:1545
msgid "Logical Volumes with associated snapshots cannot be mirrored yet."
msgstr ""

#. mirror images placement: diff HDs or anywhere
#. prompt
#: src/InputController.py:1552
msgid ""
"The primary purpose of mirroring is to protect data in the case of hard "
"drive failure. Do you want to place mirror images onto different hard drives?"
msgstr ""

#: src/InputController.py:1561
msgid ""
"Less than 3 hard drives are available with free space. Disabling mirroring."
msgstr ""

#: src/InputController.py:1566
msgid ""
"There must be free space on at least three Physical Volumes to enable "
"mirroring"
msgstr ""

#: src/InputController.py:1574
msgid ""
"The size of the Logical Volume has been adjusted to the maximum available "
"size for mirrors."
msgstr ""

#: src/InputController.py:1579
#, python-format
msgid ""
"There is not enough free space to add mirroring. Reduce size of Logical "
"Volume to at most %s, or add Physical Volumes."
msgstr ""

#: src/InputController.py:1943
msgid "Names beginning with \"snapshot\" or \"pvmove\" are reserved keywords."
msgstr ""

#: src/InputController.py:1945
msgid "Names containing \"_mlog\" or \"_mimage\" are reserved keywords."
msgstr ""

#: src/InputController.py:1947
msgid "Names beginning with a \"-\" are invalid"
msgstr ""

#: src/InputController.py:1949
msgid "Name can be neither \".\" nor \"..\""
msgstr ""

#: src/InputController.py:1955
msgid "Whitespaces are not allowed in Logical Volume names"
msgstr ""

#: src/InputController.py:1958
#, python-format
msgid "Invalid character \"%s\" in Logical Volume name"
msgstr ""

#: src/InputController.py:1978
msgid "Please specify mount point"
msgstr "Vennligst oppgi monteringspunkt"

#: src/InputController.py:2044
msgid "Do you want to upgrade ext2 to ext3 preserving data on Logical Volume?"
msgstr ""

#: src/InputController.py:2078
msgid ""
"Changing the filesystem will destroy all data on the Logical Volume! Are you "
"sure you want to proceed?"
msgstr ""

#. migration not performed, continue process with no mirroring
#: src/InputController.py:2179
msgid "Mirror not created. Completing remaining tasks."
msgstr ""

#. create mirror
#: src/InputController.py:2182
msgid ""
"Underlaying LVM doesn't support addition of mirrors to existing Logical "
"Volumes. Completing remaining tasks."
msgstr ""

#: src/InputController.py:2231
msgid "In order to add mirroring, some extents need to be migrated."
msgstr ""

#: src/InputController.py:2231
msgid "Do you want to migrate specified extents?"
msgstr ""

#: src/CommandHandler.py:55
msgid "Creating Logical Volume"
msgstr "Oppretter logisk volum"

#: src/CommandHandler.py:74 src/CommandHandler.py:91
msgid "Resizing Logical Volume"
msgstr "Endrer størrelse på logisk volum"

#: src/CommandHandler.py:124
msgid "Adding Mirror to Logical Volume"
msgstr "Legger til speiling av logisk volum"

#: src/CommandHandler.py:135
msgid "Removing Mirror from Logical Volume"
msgstr "Fjerner speil fra logisk volum"

#: src/CommandHandler.py:160
msgid "Initializing Physical Volume"
msgstr "Initierer fysisk volum"

#: src/CommandHandler.py:171
msgid "Adding Physical Volume to Volume Group"
msgstr "Legger til fysisk volum i volumgruppen"

#: src/CommandHandler.py:203
msgid "Creating Volume Group"
msgstr "Oppretter volumgruppe"

#: src/CommandHandler.py:225
msgid "Removing Volume Group"
msgstr "Fjerner volumgruppe"

#: src/CommandHandler.py:235
msgid "Removing Physical Volume"
msgstr "Fjerner fysisk volum"

#: src/CommandHandler.py:248
msgid "Removing Logical Volume"
msgstr "Fjerner logisk volum"

#: src/CommandHandler.py:263
msgid "Renaming Logical Volume"
msgstr "Gir logisk volum nytt navn"

#: src/CommandHandler.py:285
msgid "Removing Physical Volume from Volume Group"
msgstr "Fjerner fysisk volum fra volumgruppen"

#: src/CommandHandler.py:320
msgid "Migrating Extents"
msgstr ""

#: src/CommandHandler.py:332
msgid "Completing Extent Migration"
msgstr ""

#: src/CommandHandler.py:383
msgid "Rereading partition table"
msgstr "Leser partisjonstabell på nytt"

#: src/lvm_model.py:45 src/lvmui_constants.py:83
msgid "Unused"
msgstr "Ubrukt"

#: src/lvm_model.py:46
msgid "Unused Space"
msgstr "Ubrukt plass"

#: src/lvm_model.py:48
msgid "Unmounted"
msgstr "Ikke montert"

#: src/lvm_model.py:58
msgid "Volume Group Name:   "
msgstr "Navn på volumgruppe: "

#: src/lvm_model.py:59
msgid "System ID:   "
msgstr "System-ID:   "

#: src/lvm_model.py:60
msgid "Format:   "
msgstr "Format:   "

#: src/lvm_model.py:61 src/lvm_model.py:78 src/lvm_model.py:93
msgid "Attributes:   "
msgstr "Attributter:  "

#: src/lvm_model.py:62
msgid "Volume Group Size:   "
msgstr "Størrelse på volumgruppe: "

#: src/lvm_model.py:63
msgid "Available Space:   "
msgstr "Tilgjengelig plass: "

#: src/lvm_model.py:64
msgid "Total Number of Extents:   "
msgstr ""

#: src/lvm_model.py:65
msgid "Number of Free Extents:   "
msgstr ""

#: src/lvm_model.py:66
msgid "Extent Size:   "
msgstr ""

#: src/lvm_model.py:67
msgid "Maximum Allowed Physical Volumes:   "
msgstr ""

#: src/lvm_model.py:68
msgid "Number of Physical Volumes:   "
msgstr ""

#: src/lvm_model.py:69
msgid "Maximum Allowed Logical Volumes:   "
msgstr ""

#: src/lvm_model.py:70
msgid "Number of Logical Volumes:   "
msgstr ""

#: src/lvm_model.py:71
msgid "VG UUID:   "
msgstr "VG UUID:   "

#: src/lvm_model.py:73
msgid "Logical Volume Name:   "
msgstr "Navn på logisk volum:  "

#: src/lvm_model.py:74
msgid "Logical Volume Size:   "
msgstr "Størrelse på logisk volum: "

#: src/lvm_model.py:75
msgid "Number of Segments:   "
msgstr "Antal segmenter:      "

#: src/lvm_model.py:76
msgid "Number of Stripes:   "
msgstr "Antall striper:      "

#: src/lvm_model.py:77
msgid "Stripe Size:   "
msgstr "Størrelse på stripe: "

#: src/lvm_model.py:79
msgid "LV UUID:   "
msgstr "LV UUID:   "

#: src/lvm_model.py:81
msgid "Partition Type:   "
msgstr "Partisjonstype:   "

#: src/lvm_model.py:82
msgid "Size:   "
msgstr "Størrelse:"

#: src/lvm_model.py:83
msgid "Mount Point:   "
msgstr "Monteringspunkt:  "

#: src/lvm_model.py:84
msgid "Mount Point when Rebooted:   "
msgstr "Monteringspunkt etter omstart:  "

#: src/lvm_model.py:85
msgid "File System:   "
msgstr "Filsystem:     "

#: src/lvm_model.py:87
msgid "Physical Volume Name:   "
msgstr "Navn på fysisk volum:   "

#: src/lvm_model.py:88
msgid "Physical Volume Size:   "
msgstr "Størrelse på fysisk volum:   "

#: src/lvm_model.py:89
msgid "Space Used:   "
msgstr "Brukt plass:   "

#: src/lvm_model.py:90
msgid "Space Free:   "
msgstr "Ledig plass:   "

#: src/lvm_model.py:91
msgid "Total Physical Extents:   "
msgstr ""

#: src/lvm_model.py:92
msgid "Allocated Physical Extents:   "
msgstr ""

#: src/lvm_model.py:94
msgid "PV UUID:   "
msgstr "PV UUID:   "

#: src/lvm_model.py:96
msgid "Not initializable:"
msgstr "Kan ikke initieres:"

#: src/lvm_model.py:97
msgid "Extended partition"
msgstr "Utvidet partisjon"

#. Translator: the line below refers to a standard linux swap partition.
#: src/lvm_model.py:99
msgid "Swap partition currently in use"
msgstr "Aktiv swap-partisjon"

#: src/lvm_model.py:100
msgid "Foreign boot partition"
msgstr ""

#: src/lvm_model.py:101
msgid "Autopartition failure"
msgstr "Feil ved automatisk partisjonering"

#: src/lvm_model.py:259
msgid "Partition manually"
msgstr "Partisjoner manuelt"

#: src/lvm_model.py:295
msgid "Multipath device"
msgstr "Multipath-enhet"

#: src/lvm_model.py:298
msgid "Note:"
msgstr "Merknad:"

#: src/lvm_model.py:298
msgid "Initialize manually"
msgstr "Initier manuelt"

#: src/lvm_model.py:741
msgid "Clustered:   "
msgstr ""

#: src/lvm_model.py:743 src/lvm_model.py:971
msgid "True"
msgstr "Sann"

#: src/lvm_model.py:745
msgid "False"
msgstr "Usann"

#: src/lvm_model.py:803
msgid "Number of mirror images:"
msgstr ""

#: src/lvm_model.py:806
msgid "Snapshots:"
msgstr "Øyeblikksbilder:"

#: src/lvm_model.py:812
msgid "Snapshot origin:"
msgstr ""

#: src/lvm_model.py:819
msgid "Snapshot usage:"
msgstr ""

#: src/lvm_model.py:842 src/lvm_model.py:848 src/lvm_model.py:891
#: src/lvm_model.py:907
msgid "/   Root Filesystem"
msgstr "/   Rotfilsystem"

#: src/lvm_model.py:910 src/Filesystem.py:137
msgid "None"
msgstr "Ingen"

#: src/lvm_model.py:961
msgid "SCSI ID:   "
msgstr "SCSI-ID:   "

#: src/lvm_model.py:965
msgid "NONE"
msgstr "INGEN"

#: src/lvm_model.py:970
msgid "iSCSI Device:   "
msgstr "iSCSI-enhet:    "

#. INIT_ENTITY=_("Are you certain that you wish to initialize disk entity %s? All data will be lost on this device/partition.")
#: src/lvmui_constants.py:51
#, python-format
msgid ""
"All data on disk entity %s will be lost! Are you certain that you wish to "
"initialize it?"
msgstr ""

#: src/lvmui_constants.py:52
#, python-format
msgid ""
"Disk entity %s contains %s filesystem. All data on it will be lost! Are you "
"certain that you wish to initialize disk entity %s?"
msgstr ""

#: src/lvmui_constants.py:53
#, python-format
msgid ""
"Disk entity %s contains data from directory %s. All data in it will be lost! "
"Are you certain that you wish to initialize disk entity %s?"
msgstr ""

#: src/lvmui_constants.py:54
#, python-format
msgid ""
"Are you certain that you wish to initialize %s of free space on disk %s?"
msgstr ""

#: src/lvmui_constants.py:55
#, python-format
msgid ""
"You are about to initialize unpartitioned disk %s. It is advisable, although "
"not required, to create a partition on it. Do you want to create a single "
"partition encompassing the whole drive?"
msgstr ""

#: src/lvmui_constants.py:57
msgid "Reloading LVM. Please wait."
msgstr ""

#: src/lvmui_constants.py:59
#, python-format
msgid ""
"Changes will take effect after computer is restarted. If device %s is used, "
"before restart, data corruption WILL occur. It is advisable to restart your "
"computer now."
msgstr ""

#: src/lvmui_constants.py:61
msgid "Mirror Log"
msgstr ""

#: src/lvmui_constants.py:63
msgid "Unable to process request"
msgstr ""

#: src/lvmui_constants.py:65
#, python-format
msgid "%s command failed. Command attempted: \"%s\" - System Error Message: %s"
msgstr ""

#: src/lvmui_constants.py:84
msgid "Free"
msgstr "Ledig"

#: src/lvmui_constants.py:85
msgid "Free space"
msgstr "Ledig plass"

#: src/lvmui_constants.py:87
msgid "Unpartitioned space"
msgstr "Ikke partisjonert plass"

#: src/lvmui_constants.py:88
#, python-format
msgid "Unpartitioned space on %s"
msgstr "Ikke partisjonert plass på %s"

#: src/lvmui_constants.py:90
msgid "GB"
msgstr "GB"

#: src/lvmui_constants.py:91
msgid "MB"
msgstr "MB"

#: src/lvmui_constants.py:92
msgid "KB"
msgstr "KB"

#: src/lvmui_constants.py:93
msgid "Bytes"
msgstr "Bytes"

#. File System Types
#: src/lvmui_constants.py:97
msgid "No Filesystem"
msgstr "Ingen filsystem"

#: src/lvmui_constants.py:98
msgid "Ext2"
msgstr "Ext2"

#: src/lvmui_constants.py:99
msgid "Ext3"
msgstr "Ext3"

#: src/lvmui_constants.py:100
msgid "JFS"
msgstr "JFS"

#: src/lvmui_constants.py:101
msgid "MSDOS"
msgstr "MS-DOS"

#: src/lvmui_constants.py:102
msgid "Reiserfs"
msgstr "Reiserfs"

#: src/lvmui_constants.py:103
msgid "VFAT"
msgstr "VFAT"

#: src/lvmui_constants.py:104
msgid "XFS"
msgstr "XFS"

#: src/lvmui_constants.py:105
msgid "Cramfs"
msgstr "Cramfs"

#: src/Properties_Renderer.py:42
msgid "Properties for"
msgstr "Egenskaper for"

#: src/Properties_Renderer.py:43 src/renderer.py:37
msgid "Physical Volume"
msgstr "Fysisk volum"

#. UNALLOCATED_MESSAGE=_("This Volume has not been allocated \n to a Volume Group yet.")
#: src/Properties_Renderer.py:44 src/renderer.py:36
msgid "Logical Volume"
msgstr "Logisk volum"

#: src/Properties_Renderer.py:45
msgid "Unallocated Volume"
msgstr "Ikke-allokert volum"

#: src/Properties_Renderer.py:46 src/renderer.py:43
msgid "Disk Entity"
msgstr "Diskentitet"

#: src/Properties_Renderer.py:47 src/Properties_Renderer.py:48
#: src/Properties_Renderer.py:49 src/renderer.py:38
msgid "Volume Group"
msgstr "Volumgruppe"

#. UNINITIALIZED_MESSAGE=_("This extent has not yet been \n initialized for use with LVM.")
#: src/renderer.py:33
msgid "No Volume Selected"
msgstr "Ingen volum valgt"

#: src/renderer.py:34
#, fuzzy
msgid "Multiple selection"
msgstr "Flere valg"

#: src/renderer.py:39 src/Volume_Tab_View.py:48
msgid "Logical View"
msgstr "Logisk visning"

#: src/renderer.py:40 src/Volume_Tab_View.py:47
msgid "Physical View"
msgstr "Fysisk visning"

#: src/renderer.py:41
msgid "Unallocated"
msgstr "Ikke-allokert"

#: src/renderer.py:42
msgid "Uninitialized"
msgstr "Ikke initiert"

#: src/renderer.py:195
#, python-format
msgid ""
"The extents that you are attempting to select belong to a mirror log of "
"Logical Volume %s. Mirrored Logical Volumes are not yet migratable, so the "
"extents are not selectable."
msgstr ""

#: src/renderer.py:197
#, python-format
msgid ""
"The extents that you are attempting to select belong to mirror image of "
"Logical Volume %s. Mirrored Logical Volumes are not yet migratable, so the "
"extents are not selectable."
msgstr ""

#: src/renderer.py:199
#, python-format
msgid ""
"The extents that you are attempting to select belong to %s, a snapshot of %"
"s. Snapshots are not yet migratable, so the extents are not selectable."
msgstr ""

#: src/renderer.py:201
#, python-format
msgid ""
"The extents that you are attempting to select belong to a snapshot origin %"
"s. Snapshot origins are not yet migratable, so the extents are not "
"selectable."
msgstr ""

#: src/renderer.py:388
#, python-format
msgid "Snapshot of %s"
msgstr "Bilde av %s"

#: src/renderer.py:490
msgid "Origin"
msgstr "Opprinnelse"

#: src/renderer.py:496
msgid "Snapshot"
msgstr "Bilde"

#: src/renderer.py:540
msgid "extent view"
msgstr ""

#: src/Volume_Tab_View.py:44
msgid "Volume Groups"
msgstr "Volumgrupper"

#: src/Volume_Tab_View.py:45
msgid "Unallocated Volumes"
msgstr "Ikke-allokerte volumer"

#: src/Volume_Tab_View.py:46
msgid "Uninitialized Entities"
msgstr "Ikke-initierte entiteter"

#: src/Volume_Tab_View.py:233
msgid "Clustered VG"
msgstr ""

#: src/Volume_Tab_View.py:619
#, python-format
msgid "%s mirror synchronisation"
msgstr ""

#. no existing partitions, write
#: src/fdisk_wrapper.py:192
msgid "Please wait while partition is being created"
msgstr ""

#: src/Filesystem.py:20
#, python-format
msgid "Creating %s filesystem"
msgstr "Oppretter %s-filsystem"

#: src/Filesystem.py:21
#, python-format
msgid "Resizing %s filesystem"
msgstr "Endrer størrelse på %s-filsystem"

#: src/Filesystem.py:22
#, python-format
msgid "Checking %s filesystem"
msgstr "Sjekker %s-filsystem"

#: src/Filesystem.py:23
#, python-format
msgid "Upgrading %s filesystem to %s"
msgstr "Oppgraderer %s-filsystem til %s"

#: src/Filesystem.py:24
#, python-format
msgid ""
"Creation of filesystem failed. Command attempted: \"%s\" - System Error "
"Message: %s"
msgstr ""

#: src/Filesystem.py:25
#, python-format
msgid ""
"Resize of filesystem failed. Command attempted: \"%s\" - System Error "
"Message: %s"
msgstr ""

#: src/Filesystem.py:26
#, python-format
msgid ""
"Check of filesystem failed. Command attempted: \"%s\" - System Error "
"Message: %s"
msgstr ""

#: src/Filesystem.py:27
#, python-format
msgid ""
"Upgrade of filesystem failed. Command attempted: \"%s\" - System Error "
"Message: %s"
msgstr ""

#: src/Filesystem.py:143
msgid "Unknown filesystem"
msgstr "Ukjent filsystem"

#: src/Filesystem.py:387
msgid "GFS (local)"
msgstr "GFS (lokalt)"

#: src/Filesystem.py:485
msgid "GFS (clustered)"
msgstr "GFS (klynge)"

#: src/Filesystem.py:517 src/Filesystem.py:761
msgid "Cluster name contains illegal character "
msgstr ""

#: src/Filesystem.py:520 src/Filesystem.py:764
msgid "GFS name contains illegal character "
msgstr ""

#: src/Filesystem.py:523 src/Filesystem.py:767
msgid "Missing Cluster Name"
msgstr ""

#: src/Filesystem.py:526 src/Filesystem.py:770
msgid "Missing GFS Name"
msgstr ""

#: src/Filesystem.py:631
msgid "GFS2 (local)"
msgstr "GFS2 (lokalt)"

#: src/Filesystem.py:729
msgid "GFS2 (clustered)"
msgstr "GFS2 (klynge)"

#: src/Segment.py:44
msgid "Stripe"
msgstr "Stripe"

#: src/Segment.py:68
msgid "Linear Mapping"
msgstr "Lineær tilordning"

#: src/Segment.py:109
msgid "Mirror"
msgstr "Speil"

#: src/PhysicalVolume.py:142
#, python-format
msgid "Partition %s"
msgstr "Partisjon %s"

#: src/lvui.glade.h:1 src/lv_edit_props.glade.h:1 src/iscsi.glade.h:1
msgid "*"
msgstr "*"

#: src/lvui.glade.h:2
msgid "1024"
msgstr "1024"

#: src/lvui.glade.h:3
msgid "128"
msgstr "128"

#: src/lvui.glade.h:4
msgid "16"
msgstr "16"

#: src/lvui.glade.h:5
msgid "2"
msgstr "2"

#: src/lvui.glade.h:6
msgid "256"
msgstr "256"

#: src/lvui.glade.h:7
msgid "32"
msgstr "32"

#: src/lvui.glade.h:8
msgid "4"
msgstr "4"

#: src/lvui.glade.h:9
msgid "512"
msgstr "512"

#: src/lvui.glade.h:10
msgid "64"
msgstr "64"

#: src/lvui.glade.h:11
msgid "8"
msgstr "8"

#: src/lvui.glade.h:12
msgid "Add Physical Volume to VG"
msgstr "Legg til fysisk volum i volumgruppe"

#: src/lvui.glade.h:13
msgid ""
"Add to existing \n"
"Volume Group"
msgstr ""
"Legg til i eksisterende \n"
"volumgruppe"

#: src/lvui.glade.h:15
msgid "Clustered"
msgstr "Klynge"

#: src/lvui.glade.h:16
msgid ""
"Create New\n"
"Logical Volume"
msgstr ""
"Opprett nytt\n"
"logisk volum"

#: src/lvui.glade.h:18
msgid "Create Snapshot"
msgstr "Lag øyeblikksbilde"

#: src/lvui.glade.h:19
msgid ""
"Create new \n"
"Volume Group"
msgstr ""
"Opprett ny \n"
"volumgruppe"

#: src/lvui.glade.h:21
msgid "Edit Properties"
msgstr "Rediger egenskaper"

#: src/lvui.glade.h:22
msgid "Enter path of Block Device to initialize"
msgstr "Oppgi sti til blokkenhet som skal initieres"

#: src/lvui.glade.h:23
msgid ""
"Extend\n"
"Volume Group"
msgstr ""
"Utvid\n"
"volumgruppe"

#: src/lvui.glade.h:25
msgid "Extend Volume Group"
msgstr "Utvid volumgruppe"

#: src/lvui.glade.h:26
msgid "Format"
msgstr "Formatter"

#: src/lvui.glade.h:27
msgid "Initialize"
msgstr "Initier"

#: src/lvui.glade.h:28
msgid "Initialize Block Device"
msgstr "Initier blokkenhet"

#: src/lvui.glade.h:29
msgid "Initialize Entity"
msgstr "Initier entitet"

#: src/lvui.glade.h:30
msgid "Initialize _Block Device"
msgstr "Initier _blokkenhet"

#: src/lvui.glade.h:31
msgid "Kilo"
msgstr "Kilo"

#: src/lvui.glade.h:32
msgid "Logical Volume Management"
msgstr "Håndtering av logiske volum"

#: src/lvui.glade.h:33
msgid "Manage Volumes"
msgstr "Håndter volumer"

#: src/lvui.glade.h:34
msgid "Mark Volume Group as 'clustered'"
msgstr ""

#: src/lvui.glade.h:35
msgid "Maximum Logical Volumes"
msgstr "Maksimalt antall logiske volum"

#: src/lvui.glade.h:36
msgid "Maximum Physical Volumes"
msgstr "Maksimalt antall fysiske volum"

#: src/lvui.glade.h:37
msgid "Meg"
msgstr "Meg"

#: src/lvui.glade.h:38
msgid ""
"Migrate Selected\n"
"Extent(s) From Volume"
msgstr ""

#: src/lvui.glade.h:40
msgid "New Volume Group"
msgstr "Ny volumgruppe"

#: src/lvui.glade.h:41
#, fuzzy
msgid "Physical Extent Size"
msgstr "Størrelse på fysisk volum:   "

#: src/lvui.glade.h:42
msgid ""
"Remove \n"
"Logical Volume"
msgstr ""
"Fjern \n"
"logisk volum"

#: src/lvui.glade.h:44
msgid ""
"Remove Selected\n"
"Logical Volume(s)"
msgstr ""
"Fjern valgt(e)\n"
"logiske volum(er)"

#: src/lvui.glade.h:46
msgid ""
"Remove Selected\n"
"Physical Volume(s)"
msgstr ""
"Fjern valgt(e)\n"
"fysiske volum"

#: src/lvui.glade.h:48
msgid ""
"Remove Volume from\n"
"Volume Group"
msgstr ""
"Fjern volum fra\n"
"volumgruppe"

#: src/lvui.glade.h:50
msgid ""
"Remove volume \n"
"from LVM"
msgstr ""
"Fjern volum \n"
"fra LVM"

#: src/lvui.glade.h:52
msgid "Select a Volume Group to add this PV to:"
msgstr "Velg volumgruppe dette fysiske volumet skal plasseres i:"

#: src/lvui.glade.h:53
msgid "Some text"
msgstr "Tekst"

#: src/lvui.glade.h:54
msgid "Volume Group Name"
msgstr "Navn på volumgruppe"

#: src/lvui.glade.h:55
msgid "_Reload"
msgstr "Les _på nytt"

#: src/lvui.glade.h:56
msgid "_Tools"
msgstr "Verk_tøy"

#: src/lvui.glade.h:57
msgid "_iSCSI Configuration"
msgstr "_iSCSI-konfigurasjon"

#: src/lvui.glade.h:58
msgid "extend vg message:"
msgstr ""

#: src/lv_edit_props.glade.h:2
msgid ""
"4\n"
"8\n"
"16\n"
"32\n"
"64\n"
"128\n"
"256\n"
"512"
msgstr ""
"4\n"
"8\n"
"16\n"
"32\n"
"64\n"
"128\n"
"256\n"
"512"

#: src/lv_edit_props.glade.h:10
msgid "<b>Filesystem</b>"
msgstr "<b>Filsystem</b>"

#: src/lv_edit_props.glade.h:11
msgid "<b>LV Properties</b>"
msgstr ""

#: src/lv_edit_props.glade.h:12
msgid "<b>Size</b>"
msgstr "<b>Størrelse</b>"

#: src/lv_edit_props.glade.h:13
msgid "Add entry to /etc/fstab"
msgstr "Legg til oppføring i /etc/fstab"

#: src/lv_edit_props.glade.h:14
msgid "Create New Logical Volume (LV)"
msgstr "Opprett nytt logisk volum (LV)"

#: src/lv_edit_props.glade.h:15
msgid "Filesystem is not resizable"
msgstr "Filsystemet kan ikke endre størrelse"

#: src/lv_edit_props.glade.h:16
msgid "Free space in Volume Group label"
msgstr "Ledig plass i etikett for volumgruppe"

#: src/lv_edit_props.glade.h:17
msgid "Free space remaining label"
msgstr ""

#: src/lv_edit_props.glade.h:18
msgid "Kilobytes granularity"
msgstr ""

#: src/lv_edit_props.glade.h:19
msgid "LV name:"
msgstr "LV-navn:"

#: src/lv_edit_props.glade.h:20
msgid "LV size"
msgstr "LV-størrelse"

#: src/lv_edit_props.glade.h:21
msgid "LVs under snapshots are not resizable"
msgstr ""

#: src/lv_edit_props.glade.h:22
msgid "Linear"
msgstr "Lineær"

#: src/lv_edit_props.glade.h:23
msgid "Mirrored"
msgstr "Speilet"

#: src/lv_edit_props.glade.h:24
msgid "Mirrored LVs are not resizable"
msgstr ""

#: src/lv_edit_props.glade.h:25
msgid "Mount"
msgstr "Monter"

#: src/lv_edit_props.glade.h:26
msgid "Mount point:"
msgstr "Monteringspunkt:"

#: src/lv_edit_props.glade.h:27
msgid "Mount when rebooted"
msgstr "Monter ved omstart"

#: src/lv_edit_props.glade.h:28
msgid "Size beg"
msgstr ""

#: src/lv_edit_props.glade.h:29
msgid "Size end"
msgstr ""

#: src/lv_edit_props.glade.h:30
msgid "Striped"
msgstr "Stripet"

#: src/lv_edit_props.glade.h:31
msgid "Use remaining"
msgstr "Bruk gjenværende plass"

#: src/lv_edit_props.glade.h:32
msgid "stripes"
msgstr "striper"

#: src/migrate_extents.glade.h:1
msgid "<b>Destination</b>"
msgstr "<b>Mål</b>"

#: src/migrate_extents.glade.h:2
msgid "<b>Migration Policy</b>"
msgstr "<b>Migreringspolicy</b>"

#: src/migrate_extents.glade.h:3
msgid "Anywhere - not implemented"
msgstr ""

#: src/migrate_extents.glade.h:4
msgid "Automatically choose PVs to migrate to"
msgstr ""

#: src/migrate_extents.glade.h:5
msgid "Contiguous"
msgstr "Sammenhengende"

#: src/migrate_extents.glade.h:6
msgid "Destination:"
msgstr "Mål:"

#: src/migrate_extents.glade.h:7
msgid ""
"In order to remove PV, extents in use have to be migrated.\n"
"Select extents' destination and migration policy."
msgstr ""

#: src/migrate_extents.glade.h:9
msgid "Inherit"
msgstr "Arv"

#: src/migrate_extents.glade.h:10
msgid "Inherit policy from Volume Group"
msgstr ""

#: src/migrate_extents.glade.h:11
msgid "Migrate Extents"
msgstr ""

#: src/migrate_extents.glade.h:12
msgid "Migrate anywhere even if that reduces performance"
msgstr ""

#: src/migrate_extents.glade.h:13
msgid "New extents are adjacent to existing ones"
msgstr ""

#: src/migrate_extents.glade.h:14
msgid "Normal"
msgstr "Normal"

#: src/migrate_extents.glade.h:15
msgid "Only migrate extents belonging to LV"
msgstr ""

#: src/migrate_extents.glade.h:16
msgid "Use common sense"
msgstr "Bruk vanlig fornuft"

#: src/iscsi.glade.h:2
msgid "3260"
msgstr "3260"

#: src/iscsi.glade.h:3
msgid "Active iSCSI targets are selected "
msgstr ""

#: src/iscsi.glade.h:4
msgid "Add New Target"
msgstr "Legg til nytt mål"

#: src/iscsi.glade.h:5
msgid "Default port is 3260"
msgstr "Forvalgt port er 3260"

#: src/iscsi.glade.h:6
msgid "Enter a hostname (or an IP address) of new target to add"
msgstr ""

#: src/iscsi.glade.h:7
msgid "Hostname"
msgstr "Vertsnavn"

#: src/iscsi.glade.h:8
msgid "New iSCSI Target"
msgstr "Nytt iSCSI-mål"

#: src/iscsi.glade.h:9
msgid "Port"
msgstr "Port"

#: src/iscsi.glade.h:10
msgid ""
"Specify a hostname of new target \n"
"to add, and click \"OK\". "
msgstr ""

#: src/iscsi.glade.h:12
msgid "iSCSI Initiator"
msgstr "iSCSI-initiator"
